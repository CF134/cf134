#include<stdio.h>
int swap(int*x,int*y)
{
int temp;
temp=*x;
*x=*y;
*y=temp;
}
int main()
{
int a,b,*x,*y;
x=&a;
y=&b;
printf("enter two numbers\n");
scanf("%d%d",x,y);
printf("before swapping a=%d and b=%d\n",a,b);
swap(&a,&b);
printf("after swapping a=%d and b=%d\n",a,b);
return 0;
}